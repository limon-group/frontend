/** @format */

import {
   AppstoreFilled,
   CalendarFilled,
   FileTextFilled,
   HomeFilled,
   MailFilled,
   PieChartFilled,
   SettingFilled,
   SmileFilled,
} from '@ant-design/icons';

import { HomeOutlined } from '@ant-design/icons';
import bem from 'easy-bem';

const b = bem('sidebar');

export const MENU_ITEMS = [
   {
      title: 'Панель управления',
      key: '2',
      path: '/',
      icon: <HomeFilled className={b('sidebar_icon')} />,
   },
   {
      title: 'Отчеты',
      key: '3',
      path: '/stock',
      icon: <FileTextFilled className={b('sidebar_icon')} />,
   },
   {
      title: 'Аналитика',
      key: '4',
      path: '',
      icon: <PieChartFilled className={b('')} />,
   },
   {
      title: 'Входящие',
      key: '5',
      path: '',
      icon: <MailFilled className={b('sidebar_icon')} />,
   },
   {
      title: 'Каталог',
      key: '6',
      path: '/catalog',
      icon: <AppstoreFilled className={b('sidebar_icon')} />,
   },
   {
      title: 'Календарь',
      key: '7',
      path: '',
      icon: <CalendarFilled className={b('sidebar_icon')} />,
   },
   {
      title: 'Клиенты',
      key: '8',
      path: '',
      icon: <SmileFilled className={b('sidebar_icon')} />,
   },
   {
      title: 'Настройки',
      key: '9',
      path: '',
      icon: <SettingFilled className={b('sidebar_icon')} />,
   },
];

export const menuArray = [
   {
      title: <HomeOutlined />,
      key: 'homeKey',
   },
   {
      title: 'Женская одежда',
      key: 'womensClothesKey',
   },
   {
      title: 'Детская одежда',
      key: 'childrensClotheshomeKey',
   },
];
