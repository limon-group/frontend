/** @format */

import { CloseOutlined, MenuOutlined, SearchOutlined } from '@ant-design/icons';
import { Input, Layout, Menu } from 'antd';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';

import { MENU_ITEMS } from 'utils/constant';
import bem from 'easy-bem';

const Sidebar = () => {
   const b = bem('Sidebar');

   const navigate = useNavigate();

   const { pathname } = useLocation();

   const [collapsed, setCollapsed] = useState(true);
   const [selected, setSelected] = useState(pathname);

   // function for customization selected bg color
   function handleMenuItemClick(e: any) {
      setSelected(e.key);
   }

   useEffect(() => {
      MENU_ITEMS.find(item =>
         item.path === pathname ? setSelected(item.key) : ''
      );
   }, [pathname]);

   const navigateFn = (path: string) => {
      if (path === '/catalog') {
         window.open('/catalog', '_blank');
      } else {
         navigate(path);
      }
   };

   return (
      <Layout.Sider
         collapsible
         collapsed={collapsed}
         trigger={null}
         className={b()}
         width={250}
         style={{ backgroundColor: '#EBEBEB' }}
         collapsedWidth={70}
      >
         <Menu
            className={b('menu')}
            selectedKeys={[selected]}
            selectable
            mode='inline'
            onClick={e => handleMenuItemClick(e)}
         >
            {collapsed ? (
               <MenuOutlined
                  className={b('menuOutlined')}
                  disabled
                  onClick={() => setCollapsed(false)}
               />
            ) : (
               <div className={b('closeOutlined')}>
                  <CloseOutlined
                     onClick={() => setCollapsed(true)}
                     style={{ fontSize: 25 }}
                  />
                  <p style={{ marginLeft: 10, fontWeight: 'bold' }}>
                     LimonGroup
                  </p>
               </div>
            )}
            <Menu.Item
               title='Поиск'
               key={'1'}
               icon={<SearchOutlined style={{ fontSize: 20 }} />}
               style={{
                  backgroundColor: `${selected === '1' ? 'white' : ''}`,
                  color: 'black',
               }}
            >
               <Input placeholder='Поиск' bordered={false} />
            </Menu.Item>
            {MENU_ITEMS.map(NavItem => (
               <Menu.Item
                  key={NavItem.key}
                  icon={NavItem.icon}
                  style={{
                     marginTop: 15,
                     backgroundColor: `${
                        selected === NavItem.key ? 'white' : ''
                     }`,
                     color: 'black',
                  }}
                  onClick={() => {
                     setCollapsed(true);
                     navigateFn(NavItem.path);
                  }}
               >
                  {NavItem.title}
               </Menu.Item>
            ))}
         </Menu>
      </Layout.Sider>
   );
};

export default Sidebar;
