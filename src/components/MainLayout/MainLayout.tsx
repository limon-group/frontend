/** @format */

import React, { FC, useEffect, useState } from 'react';

import HeaderPart from 'components/Header/HeaderPart';
import { Layout } from 'antd';
import Sidebar from 'components/Sidebar/Sidebar';
import { useLocation } from 'react-router';

interface MainLayoutProps {
   children: React.ReactNode | React.ReactNode[];
}

const MainLayout: FC<MainLayoutProps> = ({ children }) => {
   const [islayout, setIsLayout] = useState(true);

   const { pathname } = useLocation();

   // Logic for checking is  opened Catalog , if is open there is not side/nav bar  , if is not open there are  side/nav bar

   useEffect(() => {
      if (pathname === '/catalog') {
         setIsLayout(false);
      } else {
         setIsLayout(true);
      }
   }, [pathname]);

   return (
      <>
         {islayout ? (
            <Layout hasSider>
               <Sidebar />
               <Layout>
                  <HeaderPart />
                  <Layout.Content style={{ padding: '0 50px' }}>
                     {children}
                  </Layout.Content>
               </Layout>
            </Layout>
         ) : (
            <Layout.Content>{children}</Layout.Content>
         )}
      </>
   );
};

export default MainLayout;
