/** @format */

import '../../assets/scss/scssfile/_header.scss';

import { Badge, Divider } from 'antd';
import { BellFilled, MenuOutlined } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';

import { MENU_ITEMS } from 'utils/constant';
import bem from 'easy-bem';

const HeaderPart = () => {
   const b = bem('header');

   const [isMenu, setIsMenu] = useState(true);
   const [isMenuItems, setIsMenuItems] = useState(false);

   // Window Rezise mode listener
   useEffect(() => {
      if (window.innerWidth <= 730) {
         setIsMenu(true);
      } else {
         setIsMenu(false);
      }
      if ('isMenu') {
         alert('efiohehfeuo');
      }
   }, [window.innerWidth]);

   const navigate = useNavigate();

   return (
      <header className={b()}>
         <div className={b('parent_block')}>
            <div
               className={b('block_1')}
               style={{ cursor: 'pointer' }}
               onClick={() => navigate('/')}
            >
               <div className={b('logo')} />
            </div>
            <div className={b('block_2')}>
               {isMenu ? (
                  <div>
                     <MenuOutlined
                        onClick={() => setIsMenuItems(!isMenuItems)}
                        style={{ fontSize: 25 }}
                     />
                     {isMenuItems && (
                        <div className={b('myAnimation')}>
                           {MENU_ITEMS.map(item => (
                              <>
                                 <Link
                                    to={item.path}
                                    key={item.key}
                                    onClick={() => setIsMenuItems(false)}
                                 >
                                    {item.icon}
                                    <p style={{ marginLeft: 15 }}>
                                       {item.title}
                                    </p>
                                 </Link>
                                 <Divider />
                              </>
                           ))}
                        </div>
                     )}
                  </div>
               ) : (
                  <>
                     <div className={b('child_block_1')}>
                        <Badge count={5}>
                           <BellFilled style={{ fontSize: 25 }} />
                        </Badge>
                     </div>
                     <div className={b('child_block_2')}>
                        <p>Анара</p>
                        <p>Администратор</p>
                     </div>
                  </>
               )}
            </div>
         </div>
      </header>
   );
};

export default HeaderPart;
