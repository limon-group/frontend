import {Button, Form, Input} from 'antd';
import React from 'react';

import {IFabric} from '../../interfaces';

const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 15},
};

const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};

/* const formQ = [
  { nameProduct: 'Назвние ткани' },
  { totalPrice: 'Цена' },
  { quantity: 'Количество' },
  { address: 'Адрес' },
  { color: 'Цвет' },
  { datePurchase: 'Дата закупки' },
]; */

interface IAddTextile {
    handleOk: () => void;
    handleCancel: () => void;
    handleAdd: (addData: IFabric) => void;
}

const AddFabric: React.FC<IAddTextile> = ({handleAdd, handleOk, handleCancel}) => {

    const [form] = Form.useForm()

    const onFinish = (values: any) => {
        handleAdd(values);
        handleCancel();
        form.resetFields();
    };
    return (
        <Form {...layout} name='purchase' onFinish={onFinish} validateMessages={validateMessages}>
            <Form.Item
                name={['purchase', 'nameFabric']}
                label='Назвние ткани'
                rules={[{required: true}]}
            >
                <Input/>
            </Form.Item>
            <Form.Item name={['purchase', 'totalPrice']} label='Цена'>
                <Input/>
            </Form.Item>
            <Form.Item name={['purchase', 'address']} label='Адрес'>
                <Input/>
            </Form.Item>
            <Form.Item name={['purchase', 'quantity']} label='Количество'>
                <Input/>
            </Form.Item>
            <Form.Item name={['purchase', 'color']} label='Цвет'>
                <Input/>
            </Form.Item>
            <Form.Item name={['purchase', 'datePurchase']} label='Дата закупки'>
                <Input/>
            </Form.Item>
            <Form.Item style={{marginBottom: '0px'}} wrapperCol={{...layout.wrapperCol, offset: 8}}>
                <Button type='default' style={{margin: '0 8px'}} htmlType='button' onClick={handleOk}>
                    cancel
                </Button>
                <Button type='primary' htmlType='submit'>
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
};

export default AddFabric;
