import { Button, Col, Form, Input, Row } from 'antd';
import React from 'react';

import { IFabric } from '../../interfaces';
import { updateFabricAsync } from '../../redux/Stock/StockActionCreater';
import { useAppDispatch, useAppSelector } from '../../hooks/hooks';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 15 },
};

const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};

interface IEditFabric {
  fabric: IFabric;
  handleOk: () => void;
  handleCancel: () => void;
}

const EditFabric: React.FC<IEditFabric> = ({ fabric, handleOk, handleCancel }) => {
 // const { fabric } = useAppSelector((state) => state.Stock);
  const dispatch = useAppDispatch();
  const [form] = Form.useForm()

  const onFinish = (values: any) => {
    const newData = { ...fabric, ...values.purchaseEdit };
    console.log(fabric);
    console.log(newData);
    dispatch(updateFabricAsync(newData));
    form.resetFields()
    handleCancel();
  };
  return (
    <Form
      layout='vertical'
      name='purchaseEdit'
      onFinish={onFinish}
      validateMessages={validateMessages}
    >
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name={['purchaseEdit', 'nameFabric']}
            label='Название'
            rules={[{ required: true }]}
            initialValue={fabric.nameFabric}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={['purchaseEdit', 'totalPrice']}
            label='Цена'
            initialValue={fabric.totalPrice}
          >
            <Input />
          </Form.Item>
          <Form.Item name={['purchaseEdit', 'address']} label='Адрес' initialValue={fabric.address}>
            <Input />
          </Form.Item>
          <Form.Item
            name={['purchaseEdit', 'quantity']}
            label='Количество'
            initialValue={fabric.quantity}
          >
            <Input />
          </Form.Item>
          <Form.Item name={['purchaseEdit', 'color']} label='Цвет' initialValue={fabric.color}>
            <Input />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item
            name={['purchaseEdit', 'remainder']}
            label='Остаток ткани'
            initialValue={fabric.remainder}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={['purchaseEdit', 'datePurchase']}
            label='Дата закупки'
            initialValue={fabric.datePurchase}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name={['purchaseEdit', 'isReady']}
            label='isReady'
            initialValue={fabric.isReady}
          >
            <Input />
          </Form.Item>
          <Form.Item name={['purchaseEdit', 'defect']} label='Брак' initialValue={fabric.defect}>
            <Input />
          </Form.Item>
          <Form.Item
            name={['purchaseEdit', 'dateStart']}
            label='Дата старта'
            initialValue={fabric.dateStart}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item style={{ marginBottom: '0px' }} wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
        <Button type='default' style={{ margin: '0 8px' }} htmlType='button' onClick={handleOk}>
          cancel
        </Button>
        <Button type='primary' htmlType='submit'>
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};

export default EditFabric;

