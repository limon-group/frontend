/** @format */

import { Breadcrumb, Col, Layout, Menu, Row, Space, theme } from "antd";

import { HeartOutlined } from "@ant-design/icons";
import React from "react";
import { menuArray } from "../../utils/constant";

const { Header, Content, Footer } = Layout;

const Catalog: React.FC = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <Layout className='layout' id='lemon_catalog'>
      <Header>
        <div className='logo' />
        <Menu theme='dark' mode='horizontal' defaultSelectedKeys={["1"]}>
          {menuArray.map(item => {
            return <Menu.Item key={item.key}>{item.title}</Menu.Item>;
          })}
        </Menu>
      </Header>
      <Content>
        <Breadcrumb style={{ margin: "16px 0" }}>
          <div>
            <Breadcrumb.Item>Главная</Breadcrumb.Item>
            <Breadcrumb.Item>Каталог</Breadcrumb.Item>
            <Breadcrumb.Item>Женская</Breadcrumb.Item>
          </div>

          <Space>
            <HeartOutlined /> Сохраненные товары
          </Space>
        </Breadcrumb>
        <div
          id='catalog'
          className='site-layout-content'
          style={{ background: colorBgContainer, padding: 0 }}
        >
          <Col span={24} className='main-photo odd'>
            <span>
              <h1>ЖЕНСКАЯ ОДЕЖДА</h1>
              <hr />
              <p>
                <i>
                  Компания Limon Group — это перспективное швейное производство,
                  решающее современные задачи. Наша компания является
                  производителями женской и детской одежды.
                </i>
              </p>
            </span>
          </Col>

          <Col span={24} className='items even light'>
            <div className='item_header'>
              <Space>
                <HeartOutlined />
              </Space>
              <div>
                <h3>СПОРТИВНАЯ ДВОЙКА 1290 РУБЛЕЙ</h3>
                <hr />
                <h4>ТКАНЬ: ТУРЕЦКАЯ ДВУХНИТКА</h4>
                <h4>РАЗМЕРЫ: S,M,L,XL</h4>
              </div>
            </div>

            <Row className='grid-parrent'>
              <Col className='item itema'>
                <img
                  src='/catalogIMG/1-1-sport-dvoika-turetskaya-dvuhnitka.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
              <Col className='item itemb'>
                <img
                  src='/catalogIMG/2-2-ubki-dlinnie-len.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
              <Col className='item itemc'>
                <img
                  src='/catalogIMG/2-3-ubki-dlinnie-len.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
              <Col className='item itemd'>
                <img
                  src='/catalogIMG/2-4-ubki-dlinnie-len.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
            </Row>
          </Col>

          <Col span={24} className='items odd grey'>
            <div className='item_header'>
              <div>
                <h3>СПОРТИВНАЯ ДВОЙКА 1290 РУБЛЕЙ</h3>
                <hr />
                <h4>ТКАНЬ: ТУРЕЦКАЯ ДВУХНИТКА</h4>
                <h4>РАЗМЕРЫ: S,M,L,XL</h4>
              </div>
              <Space>
                <HeartOutlined />
              </Space>
            </div>
            <Row className='grid-parrent'>
              <Col className='item itema'>
                <img
                  src='/catalogIMG/1-1-sport-dvoika-turetskaya-dvuhnitka.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
              <Col className='item itemb'>
                <img
                  src='/catalogIMG/1-2-sport-dvoika-turetskaya-dvuhnitka.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
              <Col className='item itemc'>
                <img
                  src='/catalogIMG/1-3-sport-dvoika-turetskaya-dvuhnitka.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
              <Col className='item itemd'>
                <img
                  src='/catalogIMG/1-4-sport-dvoika-turetskaya-dvuhnitka.jpg'
                  alt='sport-dvoika-turetskaya-dvuhnitka'
                />
              </Col>
            </Row>
          </Col>
        </div>
      </Content>
      <Footer style={{ textAlign: "center" }}>
        Lemon Group ©2023 Создано Light Group
      </Footer>
    </Layout>
  );
};

export default Catalog;
