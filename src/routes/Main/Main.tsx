/** @format */

import {
   Bar,
   BarChart,
   CartesianGrid,
   Legend,
   Line,
   LineChart,
   ResponsiveContainer,
   Tooltip,
   XAxis,
} from 'recharts';

import { Card } from 'antd';
import CountUp from 'react-countup';
import bem from 'easy-bem';
import { useState } from 'react';

const Main = () => {
   const b = bem('Main');

   const [focus, setFocus] = useState(0);

   // Header x-card data

   const HEADER_DATA = [
      {
         title: 'Уникальные посетители',
         sub_title: 'Посетители за текущий месяц',
         data: '1340',
      },
      {
         title: 'Время загрузки',
         sub_title: 'Среднее время загрузки страницы',
         data: '1.23',
      },
      {
         title: 'Новые выпуски',
         sub_title: 'Кол-во новых выпусков',
         data: '76',
      },
   ];

   // Column chart

   const BAR_CHART_DATA = [
      {
         name: 'Пн',
         пока_хз_че_здесь: 10000,
         количество: 2400,
      },
      {
         name: 'Вт',
         пока_хз_че_здесь: 3000,
         количество: 1398,
      },
      {
         name: 'Ср',
         пока_хз_че_здесь: 2000,
         количество: 9800,
      },
      {
         name: 'Чт',
         пока_хз_че_здесь: 2780,
         количество: 3908,
      },
      {
         name: 'Пт',
         пока_хз_че_здесь: 1890,
         количество: 4800,
      },
      {
         name: 'Сб',
         пока_хз_че_здесь: 2390,
         количество: 3800,
      },
      {
         name: 'Вс',
         пока_хз_че_здесь: 3490,
         количество: 4300,
      },
   ];
   const LINE_CHART_DATA = [
      {
         outgoing: 10000,
         incoming: 2400,
         amt: 1000,
      },
      {
         outgoing: 3000,
         incoming: 1398,
         amt: 2210,
      },
      {
         outgoing: 2000,
         incoming: 9800,
         amt: 2290,
      },
      {
         outgoing: 2780,
         incoming: 3908,
         amt: 2000,
      },
      {
         outgoing: 1890,
         incoming: 4800,
         amt: 2181,
      },
      {
         outgoing: 2390,
         incoming: 3800,
         amt: 2500,
      },
      {
         outgoing: 7000,
         incoming: 4500,
         amt: 1000,
      },
   ];

   return (
      <div className={b()}>
         <div className={b('block-1')}>
            <section className={b('header-section-1')}>
               {HEADER_DATA.map((item, index) => (
                  <div
                     className={b('header-parent-block')}
                     key={index}
                     onClick={() => setFocus(index)}
                     style={{
                        backgroundColor: focus === index ? '#444444' : '',
                     }}
                  >
                     <div className={b('header-child-block')}>
                        <p
                           className={'p1'}
                           style={{
                              color: focus === index ? 'white' : '',
                           }}
                        >
                           {item.title}
                        </p>
                        <p className={'p2'}>{item.sub_title}</p>
                     </div>
                     <p
                        className={b('number')}
                        style={{
                           color: focus === index ? 'white' : '',
                        }}
                     >
                        {/* For count animation :) */}

                        <CountUp end={Number(item.data)} duration={5} />
                     </p>
                  </div>
               ))}
            </section>
            <section className={b('header-section-2')}>
               <Card
                  title='Eженедельные поситители'
                  style={{
                     border: '1px solid gray',
                     height: '100%',
                     width: '100%',
                  }}
               >
                  <ResponsiveContainer width='100%' height={200}>
                     <BarChart data={BAR_CHART_DATA}>
                        <XAxis dataKey='name' />
                        <Bar
                           dataKey='количество'
                           stackId='a'
                           fill='#666666'
                           barSize={35}
                           cursor={'pointer'}
                        />
                        <Tooltip cursor={true} />
                        <Bar
                           dataKey='пока_хз_че_здесь'
                           stackId='a'
                           fill='#bbbbbb'
                           barSize={35}
                           cursor={'pointer'}
                        />
                     </BarChart>
                  </ResponsiveContainer>
               </Card>
            </section>
         </div>
         <div className={b('block-2')}>
            <Card
               title='Входящая/исходящая пропускная способность'
               style={{
                  border: '1px solid gray',
                  height: '100%',
                  width: '100%',
               }}
            >
               <ResponsiveContainer width='100%' height={300}>
                  <LineChart data={LINE_CHART_DATA}>
                     <Tooltip />
                     <Legend iconType='rect' iconSize={20} align='right' />
                     <CartesianGrid
                        stroke='#eaeaea'
                        horizontal
                        vertical={false}
                        strokeWidth={1}
                        repeatCount={'10'}
                     />

                     <Line
                        type='monotone'
                        strokeWidth={2}
                        dataKey='incoming'
                        stroke='#b1b1b1'
                        activeDot={{ r: 8 }}
                     />
                     <Line
                        strokeWidth={2}
                        type='monotone'
                        dataKey='outgoing'
                        stroke='#414141'
                     />
                  </LineChart>
               </ResponsiveContainer>
            </Card>
         </div>
         <div className={b('block-3')}>
            <section className={b('footer-section-1')}></section>
            <section className={b('footer-section-2')}></section>
         </div>
      </div>
   );
};

export default Main;
