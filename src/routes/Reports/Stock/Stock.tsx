/** @format */

import { Button, Modal, Popconfirm, Row, Space, Table } from "antd";
import React, { useEffect, useState } from "react";
import {
  createFabricAsync,
  deleteFabricAsync,
  fetchFabricsAsync,
} from "../../../redux/Stock/StockActionCreater";
import { useAppDispatch, useAppSelector } from "../../../hooks/hooks";

import AddFabric from "../../../components/StockForm/AddFabric";
import { ColumnsType } from "antd/es/table";
import EditFabric from "../../../components/StockForm/EditFabric";
import { IFabric } from "../../../interfaces";
import { addFabric } from "../../../redux/Stock/StockSlice";

const Stock: React.FC = () => {
  const dispatch = useAppDispatch();
  const { fabrics } = useAppSelector(state => state.Stock);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const [fabric, setFabric] = useState<IFabric>({} as IFabric);

  useEffect(() => {
    dispatch(fetchFabricsAsync());
  }, []);

  const columns: ColumnsType<IFabric> = [
    { title: "Назвние ткани", dataIndex: "nameFabric", key: "nameFabric" },
    { title: "Цена", dataIndex: "totalPrice", key: "totalPrice" },
    { title: "Address", dataIndex: "address", key: "address" },
    { title: "Количество", dataIndex: "quantity", key: "quantity" },
    { title: "Цвет", dataIndex: "color", key: "color" },
    { title: "Остаток ткани", dataIndex: "remainder", key: "remainder" },
    { title: "Дата закупки", dataIndex: "datePurchase", key: "datePurchase" },
    { title: "isReady", dataIndex: "isReady", key: "isReady" },
    { title: "Брак", dataIndex: "defect", key: "defect" },
    { title: "Дата старта", dataIndex: "dateStart", key: "dateStart" },
    {
      title: "Action",
      dataIndex: "",
      key: "x",
      render: (_, record) => (
        <Space size='middle'>
          <Button onClick={() => showModalEdit(record)}>edit</Button>
          <Popconfirm
            title='Sure to delete?'
            onConfirm={() => handleDelete(record.id)}
          >
            <Button>Delete</Button>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const showModalAdd = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleOkEdit = () => {
    setIsEditModalOpen(false);
  };
  const showModalEdit = (record: IFabric) => {
    dispatch(addFabric(record));
    setFabric(record);
    setIsEditModalOpen(prev => !prev);
  };
  const handleCancelEdit = () => {
    setIsEditModalOpen(false);
  };

  const handleDelete = (id: number) => {
    dispatch(deleteFabricAsync(id));
  };

  const handleAdd = (addData: any) => {
    const date = new Date();
    const newData: IFabric = {
      ...addData.purchase,
      remainder: 15,
      isReady: "не готов",
      defect: "нет",
      dateStart: date.toLocaleDateString("en-US"),
    };
    dispatch(createFabricAsync(newData));
  };

  // Table Pagination
  const [currenPage, setCurrenPage] = useState(1);

  const handleChange = (page: any) => {
    const { current } = page;
    if (current) {
      setCurrenPage(current);
    } else {
      const lastPage = Math.ceil(fabrics.length / 2);
      setCurrenPage(lastPage);
    }
  };

  return (
    <Row
      justify='center'
      align='middle'
      style={{
        padding: 20,
      }}
    >
      <Button onClick={showModalAdd} type='primary'>
        Добавит ткань
      </Button>
      <Table
        dataSource={fabrics}
        columns={columns}
        scroll={{ x: 1300 }}
        bordered
        pagination={{
          current: currenPage,
          pageSize: 3,
        }}
        onChange={handleChange}
      />
      <Modal
        title='Добавить ткань'
        open={isModalOpen}
        width='400px'
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <AddFabric
          handleAdd={handleAdd}
          handleOk={handleOk}
          handleCancel={handleCancel}
        />
      </Modal>
      <Modal
        title='Редактировать ткань'
        open={isEditModalOpen}
        width='600px'
        onOk={handleOkEdit}
        onCancel={handleCancelEdit}
        footer={null}
      >
        <EditFabric
          fabric={fabric}
          handleOk={handleOkEdit}
          handleCancel={handleCancelEdit}
        />
      </Modal>
    </Row>
  );
};

export default Stock;
