/** @format */

import Stock from "./Stock/Stock";
import TechTable from "./TechTable/TechTable";

const AllReports = () => {
  return (
    <>
      <Stock />
      <TechTable />
    </>
  );
};

export default AllReports;
