/** @format */

import { Row, Table } from 'antd';
import { useAppDispatch, useAppSelector } from 'hooks/hooks';
import { useEffect, useState } from 'react';

import { fetchDailyTechTableAsync } from 'redux/DailyTechTableSlice/DailyTechTableSlice';

const TechTable = () => {
   const dispatch = useAppDispatch();
   const techTableData = useAppSelector(
      state => state.DailyTechTable.techTableData
   );

   useEffect(() => {
      dispatch(fetchDailyTechTableAsync());
   }, []);

   // Table's key/columns
   const columns = [
      {
         title: 'Сотрудник',
         dataIndex: 'employee',
         key: 'employee',
      },
      {
         title: 'Модель одежды',
         dataIndex: 'model_id',
         key: 'model_id',
      },
      {
         title: 'Количество',
         dataIndex: 'quantity',
         key: 'quantity',
      },
      {
         title: 'Зарплата за один день',
         dataIndex: 'zp_one_day_work',
         key: 'zp_one_day_work',
      },
      {
         title: 'Аванс',
         dataIndex: 'avans',
         key: 'avans',
      },
      {
         title: 'Дата заполнения',
         dataIndex: 'data_work_day',
         key: 'data_work_day',
      },
   ];

   // function for calculate cuurent date
   const calculateCurrentDate = () => {
      const date = new Date();
      const formattedDate = date
         .toLocaleDateString('en-GB', {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
         })
         .toString();
      return formattedDate;
   };

   // function for calculate total sum of each items
   const totalQuantity = techTableData
      .reduce((acc, curr) => acc + parseInt(curr.quantity), 0)
      .toString();
   const totalZp = techTableData
      .reduce((acc, curr) => acc + parseInt(curr.zp_one_day_work), 0)
      .toString();
   const totalAvans = techTableData
      .reduce((acc, curr) => acc + parseInt(curr.avans), 0)
      .toString();

   const [currenPage, setCurrenPage] = useState(1);

   // function for toggle  table pages
   const handleChange = (page: any) => {
      const { current } = page;
      if (current) {
         setCurrenPage(current);
      } else {
         const lastPage = Math.ceil(techTableData.length / 2);
         setCurrenPage(lastPage);
      }
   };

   // fetch data

   return (
      <Row
         justify='center'
         align='middle'
         style={{ padding: 20, width: '100%' }}
      >
         <Table
            scroll={{ x: 1300 }}
            columns={columns}
            dataSource={[
               ...techTableData,
               {
                  model_id: '',
                  avans: totalAvans,
                  data_work_day: '',
                  employee: 'Итого:',
                  quantity: totalQuantity,
                  zp_one_day_work: totalZp,
                  key: '',
               },
            ]}
            pagination={{
               current: currenPage,
               pageSize: 5,
            }}
            onChange={handleChange}
            bordered
         />
      </Row>
   );
};

export default TechTable;
