export interface IDailyTechtable {
  employee: string;
  model_id: string;
  quantity: string;
  zp_one_day_work: string;
  avans: string;
  data_work_day: string;
  key?: string;
  render?: (value: string) => void;
}
