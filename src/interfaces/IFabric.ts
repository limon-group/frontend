

export interface IFabric {
  id: number;
  nameFabric: string;
  totalPrice: number;
  quantity: number;
  address: string;
  color: string;
  remainder: number;
  datePurchase: string;
  isReady: string;
  defect: string;
  dateStart: string;
}
