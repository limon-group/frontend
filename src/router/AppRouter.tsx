/** @format */

import { Route, Routes } from "react-router-dom";

import AllReports from "routes/Reports/AllReports";
import Catalog from "routes/Catalog/Catalog";
import Main from "routes/Main/Main";

const AppRouter = () => {
  return (
    <Routes>
      <Route path='/' index element={<Main />} />
      <Route path='/stock' index element={<AllReports />} />
      <Route path='/catalog' index element={<Catalog />} />
    </Routes>
  );
};
export default AppRouter;
