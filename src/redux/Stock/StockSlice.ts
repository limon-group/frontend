import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IFabric } from 'interfaces';

import {
  createFabricAsync,
  deleteFabricAsync,
  fetchFabricsAsync,
  updateFabricAsync,
} from './StockActionCreater';

interface IStockState {
  fabrics: IFabric[];
  fabric: IFabric;
  isLoading: boolean;
  error: string | null | undefined | unknown;
}

const initialState: IStockState = {
  fabrics: [],
  fabric: {} as IFabric,
  isLoading: false,
  error: null,
};

const stockSlice = createSlice({
  name: 'Stock',
  initialState,
  reducers: {
    addFabric: (state, action: PayloadAction<IFabric>) => {
      state.fabric = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchFabricsAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchFabricsAsync.fulfilled, (state, action: PayloadAction<IFabric[]>) => {
        state.fabrics = action.payload;
        state.isLoading = false;
      })
      .addCase(fetchFabricsAsync.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.payload;
      })
      .addCase(deleteFabricAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateFabricAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createFabricAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createFabricAsync.fulfilled, (state, action: PayloadAction<IFabric>) => {
        state.isLoading = false;
      });
  },
});
export const { addFabric } = stockSlice.actions;
export default stockSlice.reducer;
