import { IFabric } from '../../interfaces';
import axios from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';

export const fetchFabricsAsync = createAsyncThunk(
    'Stock/fetchFabricsAsync',
    async (_, thunkApi) => {
        try {
            // message.loading('загрузка', 1);
            const response = await axios.get<IFabric[]>('http://localhost:7000/fabrics');
            // message.success('Выполнено', 2);
            return response.data;
        } catch (err) {
            // message.error('Ошибка', 3);
            return thunkApi.rejectWithValue('Ошибка');
        }
    },
);
export const deleteFabricAsync = createAsyncThunk(
    'Stock/deleteFabricAsync',
    async (id: number, thunkApi) => {
        try {
            // message.loading('загрузка');
            await axios.delete<IFabric>(`http://localhost:7000/fabrics/${id}`);
            await thunkApi.dispatch(fetchFabricsAsync());
            // message.success('Выполнено');
        } catch (err) {
            // message.error('Ошибка');
            return thunkApi.rejectWithValue(err);
        }
    },
);
export const updateFabricAsync = createAsyncThunk(
    'Stock/updateFabricAsync',
    async (fabric: IFabric, thunkApi) => {
        try {
            console.log(fabric);
            // message.loading('загрузка');
            await axios.put<IFabric>(`http://localhost:7000/fabrics/${fabric.id}`, fabric);
            await thunkApi.dispatch(fetchFabricsAsync());

            // message.success('Выполнено');
        } catch (err) {
            // message.error('Ошибка');
            return thunkApi.rejectWithValue(err);
        }
    },
);
export const createFabricAsync = createAsyncThunk(
    'Stock/createFabricAsync',
    async (fabric: IFabric, thunkApi) => {
        try {
            // message.loading('загрузка');
            const response = await axios.post<IFabric>(`http://localhost:7000/fabrics`, fabric);
            await thunkApi.dispatch(fetchFabricsAsync());
            // message.success('Выполнено');

            return response.data;
        } catch (err) {
            // message.error('Ошибка');
            return thunkApi.rejectWithValue(err);
        }
    },
);
