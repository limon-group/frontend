/** @format */

import * as reduxThunk from 'redux-thunk/extend-redux';

import DailyTechTableSlice from './DailyTechTableSlice/DailyTechTableSlice';
import StockSlice from './Stock/StockSlice';
import { configureStore } from '@reduxjs/toolkit';

export const store = configureStore({
   reducer: {
      DailyTechTable: DailyTechTableSlice,
      Stock: StockSlice,
   },
   middleware: (getDefaultMiddleware: any) =>
      getDefaultMiddleware({
         serializableCheck: false,
      }),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
