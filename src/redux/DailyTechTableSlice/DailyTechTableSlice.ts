import { PayloadAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";

import { IDailyTechtable } from "interfaces/IDailyTechTable";
import axios from "axios";

interface IInitialState {
  techTableData: IDailyTechtable[];
}

const initialState: IInitialState = {
  techTableData: [],
};

// Temporary API , for cheking redux's working
const API = "https://mocki.io/v1/f43f861f-08b7-46e3-b691-cb0f83e846e3";

export const fetchDailyTechTableAsync = createAsyncThunk(
  "DTechTable/fetchDailyTechTableAsync",
  async (_, { dispatch }) => {
    try {
      const res = await axios<IDailyTechtable[]>(API);
      dispatch(setFetchDailyTechTable(res.data));
    } catch (err) {
      console.log(err);
    }
  }
);

const DailyTechTable = createSlice({
  name: "DTechTable",
  initialState,
  reducers: {
    setFetchDailyTechTable: (
      state,
      action: PayloadAction<IDailyTechtable[]>
    ) => {
      state.techTableData = action.payload;
    },
  },
});

export const { setFetchDailyTechTable } = DailyTechTable.actions;
export default DailyTechTable.reducer;
