/** @format */

import "assets/scss/app.scss";

import AppRouter from "router/AppRouter";
import MainLayout from "components/MainLayout/MainLayout";

function App() {
  return (
    <MainLayout>
      <AppRouter />
    </MainLayout>
  );
}

export default App;
