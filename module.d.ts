/** @format */

declare module "*.svg" {
  import React from "react";
  export const src: string;
}

declare module "*.png";
declare module "*.jpeg";
declare module "*.jpg";
